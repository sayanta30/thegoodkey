<?php
function tgk_custom_rules_action_info(){
    return array(
        'default_score_value' => array(
            'label' => 'Default ROOT_VALUE',
            'group' => 'Custom',
            'module' => 'tgk_custom',
            'provides' => [
                'root_value' => [
                    'type' => 'integer',
                    'label' => 'Default ROOT_VALUE',
                ],
            ],
        ),
        'update_tenant_score' => array(
            'label'=> 'Update Tenant Score',
            'group' => 'Custom',
            'module' => 'tgk_custom',
            'parameter' => array(
                'nid' => array(
                    'type' => 'integer',
                    'label' => t('User ID'),
                ),
            ),
        ),
        'send_mail_on_rental_payment' => array(
            'label'=> 'Send Mail on Payment',
            'group' => 'Custom',
            'module' => 'tgk_custom',
            'parameter' => array(
                'node' => array(
                    'type' => 'integer',
                    'label' => t('Node'),
                ),
            ),
        ),
    );
}
function default_score_value(){
    $default_root = variable_get('ROOT_VALUE');
    return array('root_value' => $default_root);
}
function update_tenant_score($nid){
    $entity = entity_load_single('node',$nid);
    $user = $entity->field_tenant['und'][0]['target_id'];
    $userNode = entity_load_single('node',$user);
    $score = $userNode->field_score['und'][0]['value'];
    $dueDate = date_create($entity->field_date_rent_is_due['und'][0]['value']);
    $currentDate = date_create($entity->field_date_rent_was_paid['und'][0]['value']);
    $daysDiffArray = date_diff($dueDate,$currentDate);
    $daysDiff = $daysDiffArray->days;
    if($daysDiff>variable_get('GRACE_PERIOD')){
        $score -= $daysDiff;
    }
    else{
        if ($score == variable_get('11_MONTH_MAX_VALUE')){
            $score+= variable_get('TWELVE_MONTH_BONUS');
        }
        else{
            $score+=variable_get('ON_TIME_BONUS');
        }
    }
    $userNode->field_score['und'][0]['value'] = $score;
    entity_save('node',$userNode);
}

function send_mail_on_rental_payment($node){

    $entity = entity_load_single('node',$node);
    $tenantUser = entity_load_single('node',$entity->field_tenant['und'][0]['target_id']);
    if(!empty($tenantUser->field_email['und'][0]['value'])){
        $tenant_email = $tenantUser->field_email['und'][0]['value'];
    }
    if(!empty($tenantUser->field_email_2['und'][0]['value'])){
        $tenant_email .= ",".$tenantUser->field_email_2['und'][0]['value'];
    }
    if(!empty($tenantUser->field_email_3['und'][0]['value'])){
        $tenant_email .= ",".$tenantUser->field_email_3['und'][0]['value'];
    }
    if(!empty($tenantUser->title_field['und'][0]['value'])){
        $tenant_name = $tenantUser->title_field['und'][0]['value'];
    }
    if(!empty($tenantUser->field_tenant_name_2['und'][0]['value'])){
        $tenant_name .="/".$tenantUser->field_tenant_name_2['und'][0]['value'];
    }
    if(!empty($tenantUser->field_tenant_name_3['und'][0]['value'])){
        $tenant_name .="/".$tenantUser->field_tenant_name_3['und'][0]['value'];
    }
    $landLordEmail = $GLOBALS['user']->mail;
    $landLordDetails = user_load($GLOBALS['user']->uid);
    $landlordName = "";
    if(isset($landLordDetails->field_first_name['und'][0]['value']) && !empty($landLordDetails->field_first_name['und'][0]['value'])){
        $landlordName.= $landLordDetails->field_first_name['und'][0]['value'];
    }
    if(isset($landLordDetails->field_last_name['und'][0]['value']) && !empty($landLordDetails->field_last_name['und'][0]['value'])){
        $landlordName.= " ".$landLordDetails->field_last_name['und'][0]['value'];
    }
    if(empty($landlordName)){
        $landlordName="Landlord";
    }

    $tenantDueDate = $entity->field_date_rent_is_due['und'][0]['value'];
    $tenantPaidDate = $entity->field_date_rent_was_paid['und'][0]['value'];
    $tenantAddress = "";
    if(isset($tenantUser->field_address_line_1['und'][0]['value']) && !empty($tenantUser->field_address_line_1['und'][0]['value'])){
        $tenantAddress.=$tenantUser->field_address_line_1['und'][0]['value'];
    }
    if(isset($tenantUser->field_address_line_2['und'][0]['value']) && !empty($tenantUser->field_address_line_2['und'][0]['value'])){
        $tenantAddress.=", ".$tenantUser->field_address_line_1['und'][0]['value'];
    }
    if(isset($tenantUser->field_city['und'][0]['value']) && !empty($tenantUser->field_city['und'][0]['value'])){
        $tenantAddress.=", ".$tenantUser->field_city['und'][0]['value'];
    }
    if(isset($tenantUser->field_state['und'][0]['value']) && !empty($tenantUser->field_state['und'][0]['value'])){
        $tenantAddress.=", ".$tenantUser->field_state['und'][0]['value'];
    }
    if(isset($tenantUser->field_zip['und'][0]['value']) && !empty($tenantUser->field_zip['und'][0]['value'])){
        $tenantAddress.=", ".$tenantUser->field_zip['und'][0]['value'];
    }
    if(empty($tenantAddress)){
        $tenantAddress = "Not Available";
    }
    if(empty($tenantUser->field_score['und'][0]['value'])){
        $tenantScore = 600;
    }
    else{
        $tenantScore = $tenantUser->field_score['und'][0]['value'];
    }

    $tenantDueDate = explode(' ',$tenantDueDate);
    $tenantDueDate = $tenantDueDate[0];
    $tenantPaidDate = explode(' ',$tenantPaidDate);
    $tenantPaidDate = $tenantPaidDate[0];
    
    rules_invoke_component('rules_send_mail_land_lord_on_payment',$landLordEmail,$tenant_name,$tenant_email,$tenantDueDate,$tenantPaidDate,$tenantAddress,$tenantScore,$landlordName);
    rules_invoke_component('rules_send_mail_tenant_on_payment',$tenant_email,$tenant_name,$tenantDueDate,$tenantPaidDate,$tenantScore,$tenantAddress);

}
