<?php

/**
 * Implements hook_views_data().
 */
function tgk_custom_views_data()
{
    $data = array();
    $data['node']['performance_status'] = array(
        'title' => t('Performance Status'),
        'help' => t('Performance status based on score'),
        'field' => array(
            'handler' => 'performance_status'
        )
    );
    return $data;
}