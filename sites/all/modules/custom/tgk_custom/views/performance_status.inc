<?php
class performance_status extends views_handler_field
{
    function option_definition()
    {
        $options = parent::option_definition();
        return $options;
    }

    function options_form(&$form, &$form_state)
    {
        parent::options_form($form, $form_state);
    }

    function query()
    {
        // do nothing -- to override the parent query.
    }

    function render($data)
    {
        $status = 'NA';
        if ($this->view->name == 'view_tenants'){
          if (isset($data->_field_data['nid']['entity']->field_score['und'][0]['value'])) {
            $score = $data->_field_data['nid']['entity']->field_score['und'][0]['value'];
            if($score >= variable_get('OUT_PERFORM')){
                $status = variable_get('OUT_PERFORM_STATE');
            }
            elseif ($score >= variable_get('CURRENT')){
                $status = variable_get('CURRENT_STATE');
            }
            elseif ($score >= variable_get('INCONSISTENT')){
                $status = variable_get('INCONSISTENT_STATE');
            }
            elseif ($score >= variable_get('UNDER_PERFORM')){
                $status = variable_get('UNDER_PERFORM_STATE');
            }
            elseif ($score >= variable_get('DELINQUENT')){
                $status = variable_get('DELINQUENT_STATE');
            }
            else{
                $status = variable_get('EVICTION_STATE');
            }
          }
          else {
            $status = 'NA';
          }
        }
        return $status;
    }
}
